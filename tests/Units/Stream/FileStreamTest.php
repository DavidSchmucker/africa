<?php declare(strict_types=1);

namespace davidschmucker\streams\Tests\Units\Stream;

use davidschmucker\streams\Stream\FileReadStream;
use davidschmucker\streams\Stream\FileWriteStream;
use PHPUnit\Framework\TestCase;

final class FileStreamTest extends TestCase
{
  private string $testFilePath = __DIR__ . '/../../Testfiles/testFile';
  private string $testPath = __DIR__ . '/../../Testfiles/';

  public function testFileReadStream(): void
  {
    try
    {
      new FileReadStream('abc');
    }
    catch(\Exception $e)
    {
      $this->assertEquals("Can't read file: abc!", $e->getMessage());
    }

    $fReadStream = new FileReadStream($this->testFilePath);

    $this->assertEquals($this->testFilePath, $fReadStream->getFilePath());
    $this->assertEquals(realpath($this->testFilePath), $fReadStream->getRealPath());

    $fReadStream->open();

    $this->assertTrue($fReadStream->isOpen());
    $this->assertFalse($fReadStream->isClosed());
    $this->assertTrue(is_resource($fReadStream->get()));
    $this->assertEquals('0', $fReadStream->read());
    $this->assertEquals(1, $fReadStream->getPosition());
    $this->assertEquals('12', $fReadStream->read(2));
    $this->assertEquals(3, $fReadStream->getPosition());
    $this->assertFalse($fReadStream->isEnd());
    $this->assertEquals('3456789', $fReadStream->read(8));
    $this->assertTrue($fReadStream->isEnd());
    $fReadStream->close();
    $this->assertFalse($fReadStream->isOpen());
    $this->assertTrue($fReadStream->isClosed());

    try
    {
      $fReadStream->get();
    }
    catch(\Exception $e)
    {
      $this->assertStringStartsWith("Stream isn't initialized yet! Please call openStream first!", $e->getMessage());
    }
  }

  public function testFileWriteStream(): void
  {
    try
    {
      new FileWriteStream($this->testPath . 'abc/abc');
    }
    catch(\Exception $e)
    {
      $this->assertStringStartsWith("Can't write file:", $e->getMessage());
    }

    touch($this->testPath . 'writeTestFile');

    $fWriteStream = new FileWriteStream($this->testPath . 'writeTestFile');

    $this->assertEquals($this->testPath . 'writeTestFile', $fWriteStream->getFilePath());
    $this->assertEquals(realpath($this->testPath . 'writeTestFile'), $fWriteStream->getRealPath());

    $fWriteStream->open();

    $this->assertTrue($fWriteStream->isOpen());
    $this->assertFalse($fWriteStream->isClosed());
    $this->assertTrue(is_resource($fWriteStream->get()));
    $fWriteStream->write('01');
    $this->assertEquals(2, $fWriteStream->getPosition());
    $fWriteStream->close();
    $this->assertFalse($fWriteStream->isOpen());
    $this->assertTrue($fWriteStream->isClosed());
    $this->assertEquals('01', file_get_contents($this->testPath . 'writeTestFile'));


    $fAppendStream = new FileWriteStream($this->testPath . 'writeTestFile', 'a');

    $fAppendStream->open();

    $this->assertTrue(is_resource($fAppendStream->get()));
    $fAppendStream->write('234');
    $this->assertEquals(3, $fAppendStream->getPosition());
    $fAppendStream->close();
    $this->assertEquals('01234', file_get_contents($this->testPath . 'writeTestFile'));

    try
    {
      $fAppendStream->get();
    }
    catch(\Exception $e)
    {
      $this->assertStringStartsWith("Stream isn't initialized yet! Please call openStream first!", $e->getMessage());
    }

    try
    {
      new FileWriteStream($this->testPath . 'writeTestFile', 'b');
    }
    catch(\Exception $e)
    {
      $this->assertStringStartsWith("Stream open Modus b isn't allowed!", $e->getMessage());
    }

    unlink($this->testPath . 'writeTestFile');


    $fWriteStream2 = new FileWriteStream($this->testPath . 'writeNewTestFile');
    $fWriteStream2->open();
    $this->assertTrue(file_exists($this->testPath . 'writeNewTestFile'));
    $fWriteStream2->close();
    unlink($this->testPath . 'writeNewTestFile');
  }
}