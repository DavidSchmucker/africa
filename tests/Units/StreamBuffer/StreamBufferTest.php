<?php declare(strict_types=1);

namespace davidschmucker\streams\Tests\Units\Stream;

use davidschmucker\streams\Stream\FileReadStream;
use davidschmucker\streams\Stream\FileWriteStream;
use davidschmucker\streams\StreamBuffer\ReadStreamBufferImpl;
use davidschmucker\streams\StreamBuffer\WriteStreamBufferImpl;
use PHPUnit\Framework\TestCase;

final class StreamBufferTest extends TestCase
{
  private string $testFilePath = __DIR__ . '/../../Testfiles/testFile';
  private string $testWriteFilePath = __DIR__ . '/../../Testfiles/testWriteFile';

  public function testReadStreamBuffer(): void
  {
    $rBuffer = new ReadStreamBufferImpl(new FileReadStream($this->testFilePath), 5);

    try
    {
      $rBuffer->readOut(0);
    }
    catch(\Exception $e)
    {
      $this->assertEquals('Read out Byte must be at least 1!', $e->getMessage());
    }
    try
    {
      $rBuffer->readOut(6);
    }
    catch(\Exception $e)
    {
      $this->assertEquals('Can\'t read out more Bytes than Buffer size!', $e->getMessage());
    }

    $this->assertEquals('01234', $rBuffer->getBuffer());
    $this->assertEquals('01', $rBuffer->readOut(2));
    $this->assertEquals('23456', $rBuffer->getBuffer());
    $this->assertEquals('2', $rBuffer->readOut(1));
    $this->assertEquals('34567', $rBuffer->getBuffer());
    $this->assertEquals('34567', $rBuffer->readOut(5));
    $this->assertEquals('89', $rBuffer->getBuffer());
    $this->assertEquals('89', $rBuffer->readOut(5));
    $this->assertEquals('', $rBuffer->getBuffer());
    $this->assertEquals('', $rBuffer->readOut(1));
    $this->assertTrue($rBuffer->driedOut());

    try
    {
      $rBuffer2 = new ReadStreamBufferImpl(new FileReadStream($this->testFilePath), 10);
      $rBuffer2->readOut(10);
      $rBuffer2->readOut(1);
    }
    catch(\Exception $e)
    {
      $this->assertEquals('End of Stream!', $e->getMessage());
    }

    try
    {
      new ReadStreamBufferImpl(new FileReadStream($this->testFilePath), 0);
    }
    catch(\Exception $e)
    {
      $this->assertEquals('Disallowed Buffer Size!', $e->getMessage());
    }
  }

  public function testWriteStreamBuffer(): void
  {
    $wBuffer = new WriteStreamBufferImpl(new FileWriteStream($this->testWriteFilePath), 5);

    try
    {
      $wBuffer->writeIn('');
    }
    catch(\Exception $e)
    {
      $this->assertEquals('Write in Byte must be at least 1!', $e->getMessage());
    }
    try
    {
      $wBuffer->writeIn('012345');
    }
    catch(\Exception $e)
    {
      $this->assertEquals('Bufferoverflow! Can\'t write into Buffer!', $e->getMessage());
    }

    $this->assertEquals('', $wBuffer->getBuffer());
    $wBuffer->writeIn('01');
    $this->assertEquals('', $wBuffer->getBuffer());
    $wBuffer->close();
    $this->assertEquals('01', file_get_contents($this->testWriteFilePath));

    unlink($this->testWriteFilePath);
  }
}