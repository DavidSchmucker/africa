<?php declare(strict_types=1);

namespace davidschmucker\streams\Tests\Units\StreamTransfer;

use davidschmucker\streams\Stream\FileReadStream;
use davidschmucker\streams\Stream\FileWriteStream;
use davidschmucker\streams\StreamBuffer\ReadStreamBufferImpl;
use davidschmucker\streams\StreamBuffer\WriteStreamBufferImpl;
use davidschmucker\streams\StreamTransfer\StreamTransfererImpl;
use PHPUnit\Framework\TestCase;

class StreamTransferTest extends TestCase
{
  private string $testFilePath = __DIR__ . '/../../Testfiles/testFile';
  private string $testWriteFilePath = __DIR__ . '/../../Testfiles/testWriteFile';

  public function testStreamTransfer(): void
  {
    $readStream = new FileReadStream($this->testFilePath);
    $writeStream = new FileWriteStream($this->testWriteFilePath);

    $readBuffer = new ReadStreamBufferImpl($readStream, 5);
    $writeBuffer = new WriteStreamBufferImpl($writeStream, 5);

    $streamTransfer = new StreamTransfererImpl($readBuffer, $writeBuffer);
    $streamTransfer->transfer(2);

    $readContent = file_get_contents($this->testFilePath);
    $writeContent = file_get_contents($this->testWriteFilePath);

    $this->assertEquals($readContent, $writeContent);

    unlink($this->testWriteFilePath);
  }
}