<?php declare(strict_types=1);

namespace davidschmucker\streams\StreamBuffer;

interface WriteStreamBuffer
{
  public function writeIn(string $data): void;

  public function close(): void;
}