<?php declare(strict_types=1);

namespace davidschmucker\streams\StreamBuffer;

use davidschmucker\streams\Stream\WriteStream;

class WriteStreamBufferImpl implements WriteStreamBuffer
{
  private WriteStream $writeStream;

  private string $buffer = '';
  private int $bufferSize;
  private int $waterMark = 0;

  public function __construct(WriteStream $writeStream, int $bufferSize = 1024)
  {
    $this->setBufferSize($bufferSize);

    $this->writeStream = $writeStream;
    $this->writeStream->open();
  }    

  private function setBufferSize(int $bufferSize): void
  {
    if($bufferSize < 2)
      throw new \Exception('Disallowed Buffer Size!');
    
    $this->bufferSize = $bufferSize;
  }

  public function getBufferSize(): int
  {
    return $this->bufferSize;
  } 

  public function getWaterMark(): int
  {
    return $this->waterMark;
  } 

  public function getBuffer(): string
  {
    return $this->buffer;
  }

  public function writeIn(string $data): void
  {
    $dataLength = strlen($data);

    if($dataLength < 1)
      throw new \Exception('Write in Byte must be at least 1!');

    if($dataLength > ($this->bufferSize - $this->waterMark))
      throw new \Exception('Bufferoverflow! Can\'t write into Buffer!');

    $this->buffer .= $data; 
    $this->emptyBuffer();
  } 

  private function emptyBuffer(): void
  {
    $this->writeStream->write($this->buffer);
    $this->buffer = '';
    $this->waterMark = 0;
  }

  public function close(): void
  {
    $this->writeStream->close();
  }

  public function clearBuffer(): void
  {
    $this->buffer = '';
  }
}