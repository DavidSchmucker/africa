<?php declare(strict_types=1);

namespace davidschmucker\streams\StreamBuffer;

interface ReadStreamBuffer
{
  public function readOut(int $byte): string;

  public function driedOut(): bool;
}