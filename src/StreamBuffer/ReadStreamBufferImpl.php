<?php declare(strict_types=1);

namespace davidschmucker\streams\StreamBuffer;

use davidschmucker\streams\Stream\ReadStream;

class ReadStreamBufferImpl implements ReadStreamBuffer
{
  private ReadStream $readStream;

  private string $buffer = '';
  private int $bufferSize;
  private int $waterMark = 0;

  public function __construct(ReadStream $readStream, int $bufferSize = 1024)
  {
    $this->setBufferSize($bufferSize);

    $this->readStream = $readStream;
    $this->readStream->open();
    $this->fillBuffer();
  }    

  private function setBufferSize(int $bufferSize): void
  {
    if($bufferSize < 2)
      throw new \Exception('Disallowed Buffer Size!');
    
    $this->bufferSize = $bufferSize;
  }

  public function getBufferSize(): int
  {
    return $this->bufferSize;
  } 

  public function getWaterMark(): int
  {
    return $this->waterMark;
  } 

  public function getBuffer(): string
  {
    return $this->buffer;
  }

  public function driedOut(): bool
  {
    return strlen($this->buffer) === 0;
  }

  public function readOut(int $byte): string
  {
    if($byte < 1)
      throw new \Exception('Read out Byte must be at least 1!');
    
    if($byte > $this->bufferSize)
      throw new \Exception('Can\'t read out more Bytes than Buffer size!');

    $returnBytes = substr($this->buffer, 0, $byte);
    $this->buffer = substr($this->buffer, $byte);
    $this->waterMark = strlen($this->buffer);

    if($this->readStream->isEnd() && $this->readStream->isClosed())
      $this->readStream->close();
    elseif(!$this->readStream->isEnd() && ($this->bufferSize > $this->waterMark))
      $this->fillBuffer();

    return $returnBytes;
  }  
  
  private function fillBuffer(): void
  {
    $this->buffer .= $this->readStream->read($this->bufferSize - $this->waterMark);
    $this->waterMark = strlen($this->buffer);
  }

  public function clearBuffer(): void
  {
    $this->buffer = '';
  }
}