<?php declare(strict_types=1);

namespace davidschmucker\streams\Stream;

interface WriteStream
{
  public function get();

  public function open(): void;

  public function isOpen(): bool;

  public function close(): void;

  public function isClosed(): bool;

  public function write(string $data): void;

  public function getPosition(): int;
}