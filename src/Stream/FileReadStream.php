<?php declare(strict_types=1);

namespace davidschmucker\streams\Stream;

class FileReadStream implements ReadStream
{
  private $stream;

  private string $filePath = '';
  private string $realPath = '';

  public function __construct(string $filePath)
  {
    $this->filePath = $filePath;
    $this->realPath = $this->checkFile($filePath);
  }

  private function checkFile(string $filePath): string
  {
    if(($realPath = realpath($filePath)) && is_readable($filePath))
      return $realPath;
    else
      throw new \Exception("Can't read file: {$filePath}!");
  }

  public function get()
  {
    if(!is_resource($this->stream))
      throw new \Exception("Stream isn't initialized yet! Please call openStream first!");

    return $this->stream;
  }

  public function open(): void
  {
    $this->stream = fopen($this->realPath, 'r');
  }

  public function isOpen(): bool
  {
    return is_resource($this->stream);
  }

  public function close(): void
  {
    fclose($this->stream);
  }

  public function isClosed(): bool
  {
    return !is_resource($this->stream);
  }

  public function read(int $byte = 1): string
  {
    return fread($this->stream, $byte);
  }

  public function isEnd(): bool
  {
    return feof($this->stream);
  }

  public function getPosition(): int
  {
    return ftell($this->stream);
  }

  public function getFilePath(): string
  {
    return $this->filePath;
  }

  public function getRealPath(): string
  {
    return $this->realPath;
  }
}