<?php declare(strict_types=1);

namespace davidschmucker\streams\Stream;

interface ReadStream
{
  public function get();

  public function open(): void;

  public function isOpen(): bool;

  public function close(): void;

  public function isClosed(): bool;

  public function read(int $byte): string;

  public function isEnd(): bool;

  public function getPosition(): int;
}