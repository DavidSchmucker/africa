<?php declare(strict_types=1);

namespace davidschmucker\streams\Stream;

use Exception;

class FileWriteStream implements WriteStream
{
  private $stream;

  private string $filePath = '';
  private string $realPath = '';

  private string $modus;
  private array $allowedModi = ['w', 'a'];

  public function __construct(string $filePath, string $modus = 'w')
  {
    $this->filePath = $filePath;
    $this->realPath = $this->checkFile($filePath);

    $this->checkModus($modus);
    $this->modus = $modus;
  }

  private function checkFile(string $filePath): string
  {
    if(file_exists($filePath) && ($realPath = realpath($filePath)) && is_writable($filePath))
      return $realPath;
    elseif(($realPath = realpath(dirname($filePath))) && is_writable(dirname($filePath)))
      return $realPath . '/' . basename($filePath);
    else
      throw new \Exception("Can't write file: {$filePath}!");
  }

  private function checkModus(string $modus): void
  {
    if(!in_array($modus, $this->allowedModi, true))
      throw new \Exception("Stream open Modus ${modus} isn't allowed!");
  }

  public function get()
  {
    if(!is_resource($this->stream))
      throw new \Exception("Stream isn't initialized yet! Please call openStream first!");

    return $this->stream;
  }

  public function open(): void
  {
    $this->stream = fopen($this->realPath, $this->modus);
  }

  public function isOpen(): bool
  {
    return is_resource($this->stream);
  }

  public function close(): void
  {
    fclose($this->stream);
  }

  public function isClosed(): bool
  {
    return !is_resource($this->stream);
  }

  public function write(string $data): void
  {
    fwrite($this->stream, $data);
  }

  public function getPosition(): int
  {
    return ftell($this->stream);
  }

  public function getFilePath(): string
  {
    return $this->filePath;
  }

  public function getRealPath(): string
  {
    return $this->realPath;
  }
}