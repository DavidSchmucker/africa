<?php declare(strict_types=1);

namespace davidschmucker\streams\StreamTransfer;

interface StreamTransferer
{
  public function transfer(int $chunkSize): void;
}