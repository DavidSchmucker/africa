<?php declare(strict_types=1);

namespace davidschmucker\streams\StreamTransfer;

use davidschmucker\streams\StreamBuffer\ReadStreamBuffer;
use davidschmucker\streams\StreamBuffer\WriteStreamBuffer;

class StreamTransfererImpl implements StreamTransferer
{
  private ReadStreamBuffer $readBuffer;
  private WriteStreamBuffer $writeBuffer;

  public function __construct(ReadStreamBuffer $readBuffer, WriteStreamBuffer $writeBuffer)
  {
    $this->readBuffer = $readBuffer;
    $this->writeBuffer = $writeBuffer;
  }

  public function transfer(int $chunkSize): void
  {
    while(!$this->readBuffer->driedOut())
    {
      $this->writeBuffer->writeIn(
        $this->readBuffer->readOut($chunkSize)
      );
    }
    $this->writeBuffer->close();
  }
}